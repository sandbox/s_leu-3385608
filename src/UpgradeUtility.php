<?php

namespace Drupal\ckeditor_templates_upgrade;

use Drupal\ckeditor_templates_upgrade\Event\AlterTemplateNeedlesEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Service description.
 */
class UpgradeUtility {

  const PROCESSED_FIELD_TYPES = [
    'text',
    'text_with_summary',
    'text_long',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs an UpgradeUtility object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  public function getTemplateNeedles() {
    $tags_to_wrap = [];
    if ($this->entityTypeManager->hasDefinition('ckeditor_template')) {
      $templates = $this->entityTypeManager->getStorage('ckeditor_template')->loadMultiple();
      foreach ($templates as $template) {
        $template_markup_value = $template->getHtml();
        $html = \is_array($template_markup_value) && isset($template_markup_value['value'])
          ? $template_markup_value['value']
          : $template_markup_value;

        if ($html) {
          $template_needle = $this->getNeedleByTemplate($html);
          if ($template_needle && !\in_array($template_needle, $tags_to_wrap)) {
            $tags_to_wrap[$template->id()] = $template_needle;
          }
        }
      }

    }

    $event = $this->eventDispatcher->dispatch(new AlterTemplateNeedlesEvent($tags_to_wrap));
    return $event->getTemplateNeedles();
  }

  public function getTemplateEmbedSelectors() : array {
    $needles = $this->getTemplateNeedles();
    $selectors = &drupal_static('ctu_template_selectors', []);
    if ($selectors) {
      return $selectors;
    }

    foreach ($needles as $template_id => $needle) {
      $classes_converted = \preg_replace_callback(
        // Based on the regex suggested at
        // https://stackoverflow.com/questions/317053/regular-expression-for-extracting-tag-attributes#answer-317081
        '/([\w|data-]+)=["\']?((?:.(?!["\']?\s+(?:\S+)=|\s*\/?[>"\']))+.)["\']?/m',
        [$this, 'classAttributeConverter'],
        $needle
      );

      $selectors[$template_id] = \preg_replace(
        ['/\R/', '~(?=<!--)([\s\S]*?)-->~mi', '/></', '/[<|>]/', '/\s\./', '/^\s+/'],
        ['', '', ' ', '', '.', ''],
        $classes_converted
      );
    }
    return $selectors;
  }

  /**
   * Callback to convert class attribute values into css class selectors.
   *
   * @return array|string|string[]|null
   */
  protected function classAttributeConverter($matches) {
    if ($matches[1] == 'class') {
      // Replace spaces inside the value of a class attribute by a dot.
      $replaced = \preg_replace('/\s/', '.', $matches[2]);
      // Prepend a dot to the first value of the class attribute.
      return \preg_replace('/^/', '.${1}', $replaced);
    }
    else {
      // Remove any other attributes than class entirely.
      // @todo Turn other attributes into xpath based css selectors.
      return \preg_replace('/' . $matches[0] . '/', '', $matches[0]);
    }
  }

  /**
   * Creates a needle consisting of some of the opening tags of the template.
   *
   * @todo Should possibly be replaced by parsing the markup of the templates
   *   using scotteh/php-dom-wrapper or plain DOMDocument and getting classes
   *   from children. The existing approach was initially constructed this way
   *   to only enqueue content that would be found by a like query condition as
   *   that would be more performant than queuing up all content. However, this
   *   wasn't completed yet.
   *
   * @todo support multiple needles for multiple top level elements per template
   */
  public function getNeedleByTemplate($markup, $full_template = '') {
    if (!\str_contains($markup, 'class=')) {
      return '';
    }

    if (!$full_template) {
      $full_template = $markup;
    }
    $current_depth = &drupal_static('template_needle', []);
    $key = \substr($full_template, 0, 30);
    if (!\array_key_exists($key, $current_depth)) {
      $current_depth[$key] = 0;
    }
    // 19 markup elements deep recursion is enough to concatenate a needle.
    if ($current_depth[$key] > 19) {
      return '';
    }
    // The default needle contains the first tag without closing bracket.
    $needle = \substr($markup, 0, \mb_strpos($markup, '>'));

    // If there's no class in the needle that's too unspecific.
    if (!\str_contains($needle, 'class=')) {
      $current_depth[$key]++;
      $needle .= '>' . $this->getNeedleByTemplate(substr($markup, \mb_strpos($markup, '>') + 1), $full_template);
    }
    return $needle;
  }

}
