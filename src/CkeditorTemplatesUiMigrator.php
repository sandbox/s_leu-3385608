<?php

namespace Drupal\ckeditor_templates_upgrade;

use Drupal\ckeditor_templates_upgrade\Event\CkeditorTemplateMigrationEvent;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Service for migration of ckeditor_templates_ui template entities.
 */
class CkeditorTemplatesUiMigrator {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * An event dispatcher instance to use for configuration events.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a TemplateMigrator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  public function migrateCkeditorTemplatesUiTemplates(array $legacy_templates, array $enabled_formats) : void {
    $template_storage = $this->entityTypeManager->getStorage('ckeditor_templates');

    foreach ($legacy_templates as $legacy_template) {
      try {
        /** @var \Drupal\ckeditor_templates\Entity\CKEditorTemplates $template */
        $template = $template_storage->create(
          $this->mapLegacyToNewTemplate($legacy_template)
        );
        $template->set('formats', $enabled_formats);
        $template->set('status', TRUE);

        // Allow other modules to amend the migrated template.
        /** @var \Drupal\ckeditor_templates\Event\CkeditorTemplateMigrationEvent $event */
        $event = $this->eventDispatcher->dispatch(new CkeditorTemplateMigrationEvent($template));

        $template = $event->getTemplate();
        $template->save();
      }
      catch (\Exception $exception) {
        \Drupal::logger('ckeditor_templates', $exception, 'Error during migration of ckeditor_templates_ui template ' . $legacy_template->id());
      }
    }
  }

  private function mapLegacyToNewTemplate(EntityInterface $legacy_template) : array {
    $values = [];
    foreach ($this->fieldMapping() as $legacy_field => $new_field) {
      $legacy_value = $legacy_template->get($legacy_field);

      // Prefix thumb_alternative paths with a slash if it's set and not a URL.
      if ($legacy_field == 'image'
        && !empty($legacy_value)
        && mb_strpos($legacy_value, '://') === FALSE
        && !\str_starts_with($legacy_value, '/')) {
        $legacy_value = '/' . $legacy_value;
      }
      $values[$new_field] = $legacy_value;
    }
    return $values;
  }

  private function fieldMapping() : array {
    return [
      'label' => 'label',
      'description' => 'description',
      'html' => 'code',
      'image' => 'thumb_alternative',
      'weight' => 'weight',
      'id' => 'id',
    ];
  }

}
