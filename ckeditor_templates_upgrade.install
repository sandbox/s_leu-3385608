<?php

use Drupal\Core\Url;
use Drupal\ckeditor_templates_upgrade\UpgradeUtility;

/**
 * @file
 * Install and update hooks for the Ckeditor Template Upgrade module.
 */

/**
 * Implements hook_install().
 */
function ckeditor_templates_upgrade_install() {
  \Drupal::service('update.update_hook_registry')->setInstalledVersion('ckeditor_templates_upgrade', 9000);

  // Collect bundles that have any of these fields for all entity types. Build
  // an array keyed by entity type and with values of associative arrays where
  // both the key and the value are bundles names containing text and
  // text_with_summary fields.
  $bundlesPerEntityType = [];

  // Collect the selectors that will be used to wrap existing template embeds
  // here in order to avoid calculating them for every item when processing
  // queue items.
  $selectors = drupal_static('ckeditor_template_upgrade_selectors');
  if (!$selectors) {
    $selectors = \Drupal::service('ckeditor_templates_upgrade.upgrade_utility')
      ->getTemplateEmbedSelectors();
  }

  /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $fieldManager */
  $fieldManager = \Drupal::service('entity_field.manager');
  $fieldMap = $fieldManager->getFieldMap();

  /** @var \Drupal\field\Entity\FieldStorageConfig $field */
  foreach ($fieldMap as $entityType => $entityTypeFieldMap) {
    foreach ($entityTypeFieldMap as $fieldName => $fieldInfo) {
      if (!in_array($fieldInfo['type'], UpgradeUtility::PROCESSED_FIELD_TYPES)) {
        continue;
      }
      foreach ($fieldInfo['bundles'] as $bundle) {
        $bundlesPerEntityType[$entityType][$bundle][] = $fieldName;
      }
    }
  }

  // Build a queue of entities we need to convert. Keep track of a total number
  // of entities so we can report a percentage in the status overview.
  $totalEntities = 0;
  /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
  $queueFactory = \Drupal::service('queue');
  $queue = $queueFactory->get('ckeditor_templates_upgrade');
  $entityTypeManager = \Drupal::entityTypeManager();

  foreach ($bundlesPerEntityType as $entityTypeId => $bundles) {
    if (empty($bundles) || empty($entityTypeId)) {
      continue;
    }
    $entityType = $entityTypeManager->getDefinition($entityTypeId);
    $bundleKey = $entityType->getKey('bundle');
    $storage = $entityTypeManager->getStorage($entityTypeId);

    foreach ($bundles as $bundle => $fields) {
      $query = $storage->getQuery();
      $query->accessCheck(FALSE);

      // Entities without bundle still have their default bundle, like for
      // example user has the user bundle. Only add the bundle condition if
      // there is a bundle key present on the entity.
      if ($bundleKey) {
        $query = $query->condition($bundleKey, $bundle);
      }
      $result = $query->execute();
      $totalEntities += count($result);
      foreach ($result as $entityId) {
        $queue->createItem([
          'entity_type' => $entityTypeId,
          'id' => $entityId,
          'selectors' => $selectors,
        ]);
      }
    }
  }

  /** @var \Drupal\Core\State\StateInterface $state */
  $state = \Drupal::service('state');
  $state->set('ckeditor_templates_upgrade_total', $totalEntities);

  \Drupal::messenger()->addStatus(t(
    'For a progress report of the upgrade of embedded ckeditor templates to media embed tags, check the <a href=":status">status overview</a>.',
    [':status' => Url::fromRoute('system.status')->toString()]));
}

/**
 * Implements hook_uninstall().
 */
function ckeditor_templates_upgrade_uninstall() {
  /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
  $queueFactory = \Drupal::service('queue');
  $entityQueue = $queueFactory->get('ckeditor_templates_upgrade');
  $entityQueue->deleteQueue();

  $state = \Drupal::service('state');
  $state->delete('ckeditor_templates_upgrade_total');
}

/**
 * Implements hook_requirements().
 */
function ckeditor_templates_upgrade_requirements($phase) {
  $requirements = [];

  if ($phase != 'runtime') {
    // Nothing to see here.
    return [];
  }

  $total = \Drupal::state()->get('ckeditor_templates_upgrade_total');
  $queue = \Drupal::queue('ckeditor_templates_upgrade');
  $entitiesLeft = $queue->numberOfItems();

  $progress = $total - $entitiesLeft;
  $requirement = [
    'title' => t('Upgrade ckeditor template embeds progress'),
  ];

  if ($progress < $total) {
    $requirement['value'] = t(
      '@progress of @total (@percentage %) of eligible entities upgraded.',
      [
        '@progress' => $progress,
        '@total' => $total,
        '@percentage' => intval($progress/$total * 100),
      ]);
    $requirement['severity'] = REQUIREMENT_WARNING;
  }
  else {
    $requirement['value'] = t(
      'All eligible entities converted. Please <a href=":uninstall">uninstall</a> the module.',
      [':uninstall' => Url::fromRoute('system.modules_uninstall')->toString()]
    );
    $requirement['severity'] = REQUIREMENT_INFO;
  }

  $requirements['ckeditor_templates_upgrade_status'] = $requirement;

  return $requirements;
}

/**
 * Migrate templates created via ckeditor_templates_ui into config entities.
 */
function ckeditor_templates_upgrade_update_9001(&$sandbox) {
  if (!\Drupal::moduleHandler()->moduleExists('ckeditor_templates_ui')) {
    $sandbox['#finished'] = TRUE;
    return;
  }

  if (!isset($sandbox['templates'])) {
    $legacy_templates = \Drupal::entityTypeManager()->getStorage('ckeditor_template')
      ->loadMultiple();
    $sandbox['templates'] = array_chunk($legacy_templates, 5);
  }

  $batch = array_shift($sandbox['templates']);
  $template_formats = array_keys(filter_formats());
  if (is_array($batch) && $template_formats) {
    \Drupal::service('ckeditor_templates_upgrade.template_migrator')
      ->migrateCkeditorTemplatesUiTemplates($batch, $template_formats);
  }

  $sandbox['#finished'] = count($sandbox['templates']) === 0;
}
